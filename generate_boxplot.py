import requests
import gzip
import shutil
import os
import pandas as pd
import numpy as np
import networkx as nx
import seaborn as sns
import matplotlib.pyplot as plt

print("Importing data from STRINGdb...")

# Download the PPI network from STRINGdb
response = requests.get("https://stringdb-static.org/download/protein.links.v11.0/9606.protein.links.v11.0.txt.gz")
with open("string_network.gz", "wb") as fh:
    fh.write(response.content)

# Extract the PPI network and remove .gz
with gzip.open("string_network.gz", "rb") as f_in, open("string_network.txt", "wb") as f_out:
  shutil.copyfileobj(f_in, f_out)

if os.path.exists("string_network.gz"):
  os.remove("string_network.gz")

# Read the PPI network
string_data = pd.read_csv("string_network.txt", sep = " ")

print("Importing data from STRINGdb: DONE.")
print("Converting data to network...")

# Correct protein IDs
string_data["protein1"] = string_data["protein1"].str.replace(r"9606.", "")
string_data["protein2"] = string_data["protein2"].str.replace(r"9606.", "")

# Filter by combined_score >= 500
string_data = string_data.loc[(string_data["combined_score"] >= 500)]

# Convert to undirected network
string_network = nx.from_pandas_edgelist(string_data, source = "protein1", target = "protein2")

print("Converting data to network: DONE.")
print("Calculating protein degree...")

# Calculate protein degree
degree_df = pd.DataFrame(dict(Degree = dict(string_network.degree)))

print("Calculating protein degree: DONE.")

# Download the number of known protein-domains per Ensembl id
# The URL corresponds to the same file as in Stockholm University Box, but in downloadable format
print("Importing protein domain data...")

response = requests.get("https://stockholmuniversity.app.box.com/index.php?rm=box_download_shared_file&shared_name=n8l0l1b3tg32wrzg2ensg8dnt7oua8ex&file_id=f_679172908592")                
with open("prot_domains.txt", "wb") as fh:
    fh.write(response.content)

# Read the protein domains file and improve column names
domains_data = pd.read_csv("prot_domains.txt", sep = "\t")
domains_data = domains_data.rename(columns = {"Pfam ID": "Pfam_ID", "Protein stable ID": "Protein_ID"})

print("Importing protein domain data: DONE.")

# Remove rows with missing data (NaN)
domains_data = domains_data.dropna()

print("Calculating number of protein domains per network protein...")

# Filter protein domains data for proteins in STRING network degree data frame
string_index = list(degree_df.index)
domains_data = domains_data.loc[domains_data["Protein_ID"].isin(string_index)]

# Count number of domains per protein ID
domain_count = domains_data["Protein_ID"].value_counts()
domain_count = domain_count.to_frame()
domain_count = domain_count.rename(columns = {"Protein_ID": "Number_domains"})

# Format data frames for merging
degree_df.index.name = "Protein_ID"
degree_df.reset_index(inplace = True)

domain_count.index.name = "Protein_ID"
domain_count.reset_index(inplace = True)

# Merge protein domain counts and STRING degree
final_df = pd.merge(degree_df, domain_count, on = "Protein_ID", how = "outer")
final_df["Number_domains"] = final_df["Number_domains"].fillna(0)

final_df["Group"] = "placeholder"

# Group proteins by degree threshold
# G1: degree > 100
# G2: degree <= 100

g1_prots = final_df[(final_df["Degree"] > 100)].copy()
g2_prots = final_df[(final_df["Degree"] <= 100)].copy()

g1_prots["Group"] = "Degree > 100"
g2_prots["Group"] = "Degree <= 100"

final_df_grouped = pd.concat((g1_prots, g2_prots))

print("Calculating number of protein domains per network protein: DONE.")

print("Generating the box plot...")

# Generate the box plot
final_boxplot = sns.boxplot(x = "Group", y = "Number_domains",
                            data = final_df_grouped, palette = "Set1", showfliers = False)
fig = final_boxplot.get_figure()
fig.savefig("protein_domains_vs_string_degree.png")

print("Generating the box plot: DONE.")
