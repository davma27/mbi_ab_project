# MBI_AB_project

#### Project: Are well-connected proteins more multi-faceted than others?

MedBioInfo Applied Bioinformatics Project (June 2020).

Author: David Martínez.

#### Requirements:
The pipeline has been tested with the following software and packages/modules.
- Python 3.6.9
  - requests 2.24.0
  - gzip 1.6
  - shutil
  - os
  - pandas 1.0.4
  - numpy 1.18.5
  - networkx 2.4
  - seaborn 0.10.1
  - matplotlib 3.2.1
- GNU Make 4.1

#### Specific settings:
- Input objects downloaded are kept.
- Proteins with no domains registered in Pfam are kept and set to 0 domains.
- Sections are run sequentially and a message is printed at the start and at completion.
- Box plot outliers are removed for better visualization.

#### Potential improvements:
- Check if input files are already downloaded.
- Remove all input files from directory after finishing.
- Better and shorter Python script/s.